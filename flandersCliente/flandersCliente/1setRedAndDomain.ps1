#=================================================================
#======================ConfigInicialClienteFlanders=======================
#Configurar una IP estática
$ipaddress = "10.0.16.2"
$dnsaddress = "10.0.16.1"
$newComputerName = "FLANDERSW10"
$DomainName = "flanders.local"
#Configuraciones para entrar al dominio
$userCore = "FLANDERS\Elsa"
$pass4UserCore = "hola123.,"


#############################################################################
# Automatizado
#Desabilitar ipv6
Disable-NetAdapterBinding -Name * -ComponentID ms_tcpip6 -PassThru

New-NetIPAddress -InterfaceAlias "Ethernet 2" -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
Set-DnsClientServerAddress -InterfaceAlias "Ethernet 2" -ServerAddresses $dnsaddress

#Firewall config
netsh advfirewall Set allprofiles State Off 

#Creamos una excepción al protocolo ICMP para probar conectividad
netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

$passCore = ConvertTo-SecureString -String $pass4UserCore -AsPlainText -Force
$CredentialCore = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userCore, $passCore

$hostname = hostname
Add-Computer -DomainName $DomainName -ComputerName $hostname -NewName $newComputerName -Credential $CredentialCore -Restart