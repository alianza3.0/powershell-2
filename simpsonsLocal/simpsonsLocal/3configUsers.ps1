#=========================================================================
#============================ConfigHostADsimpsons==========================
#Para los hosts en active directory
#Para añadir una Unidad Organizacional
New-ADOrganizationalUnit -Name "Familia" -Path "DC=simpsons,DC=local"

#Para ver las unidades organizacionales existentes
Get-ADOrganizationalUnit -Filter * | Ft

#Añadimos una variable con la contraseña a usar
$passGenerica = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force

New-ADUser -Name "Homero" -AccountPassword $passGenerica -DisplayName "Homero" `
-Enabled $True -GivenName "Homero" -Server "simpsons.local" `
-UserPrincipalName "homero@simpsons.local" -Path "OU=Familia,DC=simpsons,DC=local"

New-ADUser -Name "Marge" -AccountPassword $passGenerica -DisplayName "Marge" `
-Enabled $True -GivenName "Marge" -Server "simpsons.local" `
-UserPrincipalName "marge@simpsons.local" -Path "OU=Familia,DC=simpsons,DC=local"

New-ADUser -Name "Maggie" -AccountPassword $passGenerica -DisplayName "Maggie" `
-Enabled $True -GivenName "Maggie" -Server "simpsons.local" `
-UserPrincipalName "maggie@simpsons.local" -Path "OU=Familia,DC=simpsons,DC=local"

New-ADUser -Name "Abuelo" -AccountPassword $passGenerica -DisplayName "Abuelo" `
-Enabled $True -GivenName "Abuelo" -Server "simpsons.local" `
-UserPrincipalName "abuelo@simpsons.local" -Path "OU=Familia,DC=simpsons,DC=local"

New-ADUser -Name "Bart" -AccountPassword $passGenerica -DisplayName "Bart" `
-Enabled $True -GivenName "Bart" -Server "simpsons.local" `
-UserPrincipalName "bart@simpsons.local" -Path "OU=Familia,DC=simpsons,DC=local"

New-ADUser -Name "Lisa" -AccountPassword $passGenerica -DisplayName "Lisa" `
-Enabled $True -GivenName "Lisa" -Server "simpsons.local" `
-UserPrincipalName "lisa@simpsons.local" -Path "OU=Familia,DC=simpsons,DC=local"

#Para ver la info de cada usuario
Get-ADUser -Filter * | Ft

<#Para tener un usuario con permisos de administrador, hay que añadirlo a los 
mismos a los que pertenece el usuario Administrator 
#>

#Para obtener los grupos a los que es miembro el usuario Administrator
Get-ADPrincipalGroupMembership Administrator | select name

#Añadiendo al usuario Marge a los grupos para ser Admin
net group "Domain Users" Marge /ADD /DOMAIN
net group "Schema Admins" Marge /ADD /DOMAIN
net group "Enterprise Admins" Marge /ADD /DOMAIN
net group "Domain Admins" Marge /ADD /DOMAIN
net group "Group Policy Creator Owners" Marge /ADD /DOMAIN

<#Como el grupo Administrators lo tratamos como un grupo local, se usa 
este comando para añadir a Marge al grupo restante
#>
net localgroup "Administrators" Marge /ADD

#Corroboramos 
Get-ADPrincipalGroupMembership Marge | select name

#Desactivamos el usuario Administrator
Disable-ADAccount -Identity "Administrator"

#Cambiar nombre del administrador
#Rename-LocalUser -Name "Administrador" -NewName "Marge"

#Creando el grupo 
New-ADGroup -Name "ClubDeLosNoHomeros" -GroupCategory Security `
-GroupScope Global -DisplayName "ClubDeLosNoHomeros" -Description "NoHomeros"

#Corroboramos
Get-ADGroup -Identity "ClubDeLosNoHomeros"

#Añadimos los usuarios al grupo
Add-ADGroupMember -Identity "ClubDeLosNoHomeros" -Members Lisa,Marge,Abuelo,Bart,Maggie