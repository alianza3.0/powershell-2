#========================Configuracion IIS/CA===========================
#Para poder instalar las subfeatures necesarias de IIS, necesitamos conexion a internet
#Para esto, activamos el DHCP
Set-NetIPInterface -InterfaceAlias "Ethernet" -DHCP Enable
$dnsaddress = "8.8.8.8"
Set-DnsClientServerAddress -InterfaceAlias Ethernet -ServerAddresses $dnsaddress

#Descargamos lo que necesitamos
Find-PackageProvider -Name Nuget
Get-PackageSource

Install-PackageProvider -Name NuGet -MinimumVersion "2.8.5.208" -Force
Install-WindowsFeature -Name Web-Server -IncludeManagementTools -IncludeAllSubFeature
Add-WindowsCapability -Name "Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0"

#Una vez configurados los roles, instalamos PSPKI
Install-Module -Name PSPKI -RequiredVersion 3.2.7.0  -SkipPublisherCheck -Force

#Checar lo que se instalo con el comando anterior
Get-WindowsFeature -Name Web-Server | Where-Object installed

#Importar y ver el contenido del modulo WebAdministration
Import-Module WebAdministration
Get-Module -Name WebAdministration -ListAvailable

#Ver el modulo IIAAdministration
Get-Module -Name IISAdministration

#El numero total de comandos para WebAdministration
Get-Command -Module WebAdministration | Measure-Object | Select-Object -Property Count

Import-module ServerManager
Add-WindowsFeature Adcs-Cert-Authority -includeManagementTools

$userCore = "SIMPSONS\Administrator"
$passCore = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force
$CredentialCore = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userCore, $passCore

#--------------------------Advertencia, cambiar a config estática------------------

$ipaddress = "10.0.18.1"
$dnsaddress = "127.0.0.1"
New-NetIPAddress -InterfaceAlias Ethernet -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18

#Configuramos los roles de la CA
Install-AdcsCertificationAuthority -Credential $CredentialCore -CAType EnterpriseRootCA `
-CryptoProviderName "RSA#Microsoft Software Key Storage Provider" `
-KeyLength 4096 -HashAlgorithmName SHA256 `
-CaCommonName "SIMPSONSPC-CA" -CADistinguishedNameSuffix "DC=simpsons.local" `
-OverwriteExistingKey -OverwriteExistingDatabase -OverwriteExistingCAinDS -Force

Get-Command -Module AdcsAdministration

Install-WindowsFeature ADCS-Web-Enrollment

Install-AdcsWebEnrollment -Force

Install-AdcsEnrollmentPolicyWebService -AuthenticationType Kerberos -Force

Install-AdcsEnrollmentWebService -Force

#Cargamos los comandos que usaremos
Get-Command -Module PSPKI

Install-WindowsFeature ADCS-Web-Enrollment
Install-AdcsWebEnrollment -Force

#Importar WebAdministration manualmente, esto carga las funciones dentro del modulo
#esto nos permite ver aspectos del web server
Get-PSProvider -PSProvider WebAdministration

#Ver el contenido de la unidad 
Get-ChildItem -Path IIS:\

#Ver los sitios en IIS, igual se puede hacer con un Get-Website
Get-ChildItem -Path IIS:\Sites 

#Crear un nuevo sitio
mkdir $env:systemdrive\inetpub\wwwroot\doh
New-Website -Name "doh" -Port 80 -HostHeader "doh.simpsons.local" `
-PhysicalPath "$env:systemdrive\inetpub\wwwroot\doh"

mkdir $env:systemdrive\inetpub\wwwroot\moes
New-Website -Name "moes" -Port 80 -HostHeader "moes.simpsons.local" `
-PhysicalPath "$env:systemdrive\inetpub\wwwroot\moes"

#Redireccionar el sitio doh a moes
$doh = "doh.simpsons.local"
$moe = "http://moes.simpsons.local"
Set-WebConfiguration system.webServer/httpRedirect "IIS:\Sites\$doh" `
-Value @{enabled="true";destination="$moe";exactDestination="true";httpResponseStatus="Found"}

#Obtener sitios y su redirectionamiento para verificar que se establecio el redireccionamiento
Get-Website | select name,@{name='destination';e={(Get-WebConfigurationProperty `
-filter /system.webServer/httpRedirect -name "destination" `
-PSPath "IIS:\Sites\$($_.name)").value}}

#Creacion de regidstro DNS el moe y doh
#nombre del registro
$namereg = 'moes' 
#nombre de la zona en la que se hace el registro
$zone = 'simpsons.local'
#direccion ip para el registro
$ipreg = '10.0.18.1'
#tiempo de vida
$ttl = '01:00:00'
Add-DnsServerResourceRecordA -Name $namereg -ZoneName $zone -AllowUpdateAny `
-IPv4Address $ipreg -TimeToLive $ttl

cp "C:\inetpub\wwwroot\iisstart.htm" "C:\inetpub\wwwroot\moes\iisstart.htm"
cp "C:\inetpub\wwwroot\iisstart.png" "C:\inetpub\wwwroot\moes\iisstart.png"

$namereg = 'doh' 
#nombre de la zona en la que se hace el registro
$zone = 'simpsons.local'
#direccion ip para el registro
$ipreg = '10.0.18.1'
#tiempo de vida
$ttl = '01:00:00'
Add-DnsServerResourceRecordA -Name $namereg -ZoneName $zone -AllowUpdateAny `
-IPv4Address $ipreg -TimeToLive $ttl

#Colocamos un archivo .htm para que cargue el servicio en el navegador
cp "C:\inetpub\wwwroot\iisstart.htm" "C:\inetpub\wwwroot\doh\iisstart.htm"
cp "C:\inetpub\wwwroot\iisstart.png" "C:\inetpub\wwwroot\doh\iisstart.png"