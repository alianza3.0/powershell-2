#========================================================================
#=========================AñadirConditionalForwarder=====================
#Añadimos el forwarding
Add-DnsServerConditionalForwarderZone -Name "flanders.local" `
-ReplicationScope "Forest" -MasterServers 10.0.16.1

#========================================================================
#===========================Configuracion DHCP===========================
#Script DHCP
Install-WindowsFeature -Name dhcp
Install-WindowsFeature -Name rsat-dhcp
netsh dhcp add securitygroups
Restart-Service dhcpserver
<#verificar que se instalaron bien los modulos para DHCP
Get-WindowsFeature -Name *dhcp*
#>
#se crea el rango del scope para la red de simpsons
Add-dhcpserverv4scope -name "Simpsons" -startrange 10.0.18.1 -Endrange 10.0.18.254 `
-subnetmask 255.255.255.0 -Description "Simpsons.local"

#Para corroborar
Get-dhcpserverv4scope

#se reserva la ip 10.0.18.1 al servidor de Simpsons.local
Add-DhcpServerv4Reservation -ScopeId 10.0.18.0 -IPAddress 10.0.18.1 `
-ClientId "00-15-5D-C8-65-1E" -Description "Server-Simpsons.local"

#Se reserva la ip 10.0.18.2 al host DonBarredora de Simpsons.local
Add-DhcpServerv4Reservation -ScopeId 10.0.18.0 -IPAddress 10.0.18.2 `
-ClientId "00-15-5D-C8-65-1F" -Description "DonBarredora"

#Para corroborar la reservación de dir IP
Get-DhcpServerv4Reservation -ScopeId 10.0.18.0

#Para activar el DHCP en los equipos
# Set-NetIPInterface -InterfaceAlias "Ethernet" -DHCP Enable
