#=================================================================
#======================ConfigInicialClienteFlanders=======================
#Configurar una IP estática
$ipaddress = "10.0.18.2"
$dnsaddress = "10.0.18.1"
$newComputerName = "donbarredora"
$DomainName = "simpsons.local"
#Configuraciones para entrar al dominio
$userCore = "SIMPSONS\Marge"
$pass4UserCore = "hola123.,"


#############################################################################
# Automatizado
#Desabilitar ipv6
Disable-NetAdapterBinding -Name * -ComponentID ms_tcpip6 -PassThru

New-NetIPAddress -InterfaceAlias "Ethernet" -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
Set-DnsClientServerAddress -InterfaceAlias "Ethernet" -ServerAddresses $dnsaddress

#Firewall config
netsh advfirewall Set allprofiles State Off 

#Creamos una excepción al protocolo ICMP para probar conectividad
netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

#Para cambiar el nombre al equipo
Rename-Computer -NewName $newComputerName -Restart

echo "************************"
echo "************************"