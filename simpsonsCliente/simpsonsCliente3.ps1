#Se crea la credencial de la maquina a la que vamos a copiar nuestro msi
$cw10 = Get-Credential
#se almacena el nombre de la maquina
$eqw10 = 'W10'
#se guarda la sesion en una variable 
$sesion = New-PSSession -VMName $eqw10 -Credential $cw10
#copiamos el msi a la maquina virtual
copy -Path C:\Users\Administrator\Documents\chrome.msi -Destination C:\chrome.msi -ToSession $sesion
# Se utiliza el comando para correrlo desde la máquina base
Invoke-Command -ScriptBlock { Set-ExecutionPolicy Unrestricted } -VMName $eqw10 -Credential $cw10
Invoke-Command -ScriptBlock { .\chrome.msi /quiet } -VMName $eqw10 -Credential $cw10