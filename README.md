# Tarea PowerShell Script
## Scripts : 
 - Primero se configuran servidores padre.
   - simpsons.local
   - flanders.local

 - Para la configuración de los padres se requiere entrar a la carpeta correspondiente.
	- Carpeta: simpsonLocal
	- Carpeta: flandersLocal 
 
 - Una vez dentro de la carpeta, se ejecutan los scripts en el oreden en que son presentados.
 - Después de configurar los servidores padre se realiza lo mismo para los servidores de los clientes.
	 - simpsonsCliente
	 - flanders.Cliente
 - En la carpeta de **src** se encuentra el archivo msi con el que se instala **Google Chrome** en el equipo de **donbarredora**.
	>El archivo msi se debe encontrar en la ubicación **C:\Users\Administrator\Documents\chrome.msi** para su buen funcionamiento.

    > NOTA: Al clonar el repositorio se debe conservar la estructura establecida para el correcto funcionamiento del mismo.