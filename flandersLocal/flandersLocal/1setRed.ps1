#======================ConfigInicialSimpson=======================
#Configurar una IP estática
$ipaddress = "10.0.16.1"
$dnsaddress = "127.0.0.1"
$machineName = "FLANDERSPC"


New-NetIPAddress -InterfaceAlias Ethernet -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
Set-DnsClientServerAddress -InterfaceAlias Ethernet -ServerAddresses $dnsaddress

#Para deshabilitar el protocolo IPv6
Get-NetAdapterBinding -DisplayName "Internet protocol Version 6 (TCP/IPv6)"
Disable-NetAdapterBinding -Name Ethernet -ComponentID ms_tcpip6 -PassThru

#Firewall config
netsh advfirewall Set allprofiles State Off 
#Creamos una excepción al protocolo ICMP para probar conectividad
netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

#Para ver el nombre del equipo
#hostname

#Para ver la config IP
#ipconfig /all

#Para cambiar el nombre al equipo
Rename-Computer -NewName $machineName -Restart

echo "************************"
echo "************************"
#Tras el reinicio, accedemos de nuevo
#Enter-PSSession -VMName WinCore-Al -Credential $CredentialCore