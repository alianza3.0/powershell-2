#=========================================================================
#============================ConfigHostADflanders==========================
#Para los hosts en active directory
#Para añadir una Unidad Organizacional
New-ADOrganizationalUnit -Name "Familia" -Path "DC=flanders,DC=local"

#Para ver las unidades organizacionales existentes
Get-ADOrganizationalUnit -Filter * | Ft

#Añadimos una variable con la contraseña a usar
$passGenerica = ConvertTo-SecureString -String "hola123.," -AsPlainText -Force

New-ADUser -Name "Ned" -AccountPassword $passGenerica -DisplayName "Ned" `
-Enabled $True -GivenName "Ned" -Server "flanders.local" `
-UserPrincipalName "Ned@flanders.local" -Path "OU=Familia,DC=flanders,DC=local"

New-ADUser -Name "Elsa" -AccountPassword $passGenerica -DisplayName "Elsa" `
-Enabled $True -GivenName "Elsa" -Server "flanders.local" `
-UserPrincipalName "elsa@flanders.local" -Path "OU=Familia,DC=flanders,DC=local"

#Para ver la info de cada usuario
Get-ADUser -Filter * | Ft

<#Para tener un usuario con permisos de administrador, hay que añadirlo a los 
mismos a los que pertenece el usuario Administrator 
#>

#Para obtener los grupos a los que es miembro el usuario Administrator
Get-ADPrincipalGroupMembership Administrator | select name

#Añadiendo al usuario Elsa a los grupos para ser Admin
net group "Domain Users" Elsa /ADD /DOMAIN
net group "Schema Admins" Elsa /ADD /DOMAIN
net group "Enterprise Admins" Elsa /ADD /DOMAIN
net group "Domain Admins" Elsa /ADD /DOMAIN
net group "Group Policy Creator Owners" Elsa /ADD /DOMAIN

<#Como el grupo Administrators lo tratamos como un grupo local, se usa 
este comando para añadir a Elsa al grupo restante
#>
net localgroup "Administrators" Elsa /ADD

#Corroboramos 
Get-ADPrincipalGroupMembership Elsa | select name

#Desactivamos el usuario Administrator
Disable-ADAccount -Identity "Administrator"